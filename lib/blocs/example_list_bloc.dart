import 'dart:convert';

import 'package:http/http.dart';
import 'package:rxdart/rxdart.dart';

import '../data/constants/dummy.dart';
import '../data/models/example_model.dart';
import '../data/resources/repository.dart';

class ExampleListBloc {
  final _repository = Repository();

  // ignore: close_sinks
  final _exampleList = PublishSubject<List<ExampleModel>>();

  Observable<List<ExampleModel>> get getExampleList => _exampleList.stream;

  fetchExampleList() async {
    /**
     * this is an example code for use dummy data
     */
    Future.delayed(const Duration(milliseconds: 1000), () {
      List<ExampleModel> dummyData = exampleModelList;
      _exampleList.sink.add(dummyData);
    });
    return;
    /**
     * Example Real Fetch Data From Web Service
     */
    Response _response = await _repository.fetchExampleList({});
    if (_response.statusCode == 200) {
      dynamic _body = json.decode(_response.body);
      _exampleList.sink.add(ExampleModel.parsJson(_body['data']));
    } else {
      // TODO What you want
    }
  }
}

final exampleListBloc = ExampleListBloc();
